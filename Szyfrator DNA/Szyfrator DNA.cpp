#include <iostream>
#include <fstream>
#include <string>
#include <ctime>
#include <cmath>
using namespace std;
    string ToDNA(string cyfry)
    {
           string litery;
           for(int i=0;i<cyfry.length();i++)
           {
                   switch(cyfry[i])
                   {
                                   case '0':
                                        litery.append(1,'A');
                                        break;
                                   case '1':
                                        litery.append(1,'G');
                                        break;
                                   case '2':
                                        litery.append(1,'U');
                                        break;
                                   case '3':
                                        litery.append(1,'C');
                                        break;
                   }
           }
           return litery;
    }
    string Czworkowa(int liczba)
    {
           string wynik,kyniw;
           int temp;
           char ztempa;
           for(int i=1;i<=3;i++)
           {
                   if(liczba<pow((double)4,i))
                         wynik.append(1,'0');
           }
           while(liczba!=0)
           {
                           temp=liczba%4;
                           liczba/=4;
                           ztempa=temp+48;
                           kyniw.append(1,ztempa);
           }
           for(int i=0;i<kyniw.length();i++)
           {
                   wynik.append(1,kyniw[kyniw.length()-1-i]);
           }
           return wynik;
    }
string Zakoduj(string tekst)
{
     int dl=tekst.length();
     int cecha;
     char litera;
     string temp4,cyfrowy;
     for(int i=0;i<dl;i++)
     {
             litera=(char)tekst[i];
             cecha=(int)litera+128;
             temp4=Czworkowa(cecha);
             cyfrowy.append(temp4);
     }
     return ToDNA(cyfrowy);
}
    void FromDNA(string &cyfry, char znak)
    {
         switch(znak)
         {
                     case 'A':
                          cyfry.append(1,'0');
                          break;
                     case 'G':
                          cyfry.append(1,'1');
                          break;
                     case 'U':
                          cyfry.append(1,'2');
                          break;
                     case 'C':
                          cyfry.append(1,'3');
                          break;
         }
    }
    double Dziesietny(string czwork)
    {
           int liczba;
           double wynik;
           for(int i=0;i<4;i++)
           {
                   liczba=czwork[i]-48;
                   wynik+=liczba*(pow((double)4,3-i));
           }
           return wynik;
    }
    void Litery(string &tekst, int liczba)
    {
         char znak=liczba;
         tekst.append(1,znak);
    }
string Odkoduj(string tekst)
{
       int dl=tekst.length();
       double cecha;
       string temp,temp10,gotowe;
       for(int i=0;i<dl;i+=4)
       {
              FromDNA(temp,tekst[i+0]);
              FromDNA(temp,tekst[i+1]);
              FromDNA(temp,tekst[i+2]);
              FromDNA(temp,tekst[i+3]);
              cecha=Dziesietny(temp)-128;
              Litery(gotowe,cecha);
              temp.clear();
       }
       return gotowe;
}
    void CzyscWklej(fstream *plik)
    {
         plik->close();
         remove("wklej.txt");
         plik->open("wklej.txt", ios::out|ios::trunc);
    }
void CzyCzyscWklej(fstream *plik)
{
     char dec;
     cout<<"Wyczyscic wklej.txt? (T/N)\n";
     cin>>dec;
     if(dec=='T'||dec=='t')
     {
         CzyscWklej(plik);
     }
}
    string Koniec()
    {
         srand(time(NULL));
         string ret;
         int los=rand()%3;
         switch(los)
         {
                    case 0:
                         ret="UAA";
                         break;
                    case 1:
                         ret="UAG";
                         break;
                    case 2:
                         ret="UGA";
                         break;
         }
         return ret;
    }
void Zapis(string tekst,bool kier)
{
     fstream plikdo;
     plikdo.open("gotowe.txt", ios::out|ios::trunc);
     if(kier)
     {
             cout<<endl<<tekst<<endl<<endl;
             plikdo<<tekst;
     }
     else
     {
         string kon=Koniec();
         cout<<"AUG"<<tekst<<kon<<endl<<endl;
         plikdo<<"AUG"<<tekst<<kon;
     }
     plikdo.close();
     
}
int main()
{
    fstream plikz;
    string calosc,gotowiec;
    bool ZDNA;
    plikz.open("wklej.txt", ios::in|ios::out);
    if(!plikz.is_open())
    {
                       cout<<"BLAD! Nie udalo sie otworzyc wklej.txt\n";
                       system("PAUSE");
                       return 0;
    }
    remove("gotowe.txt");
    getline(plikz,calosc);
    if(calosc.length())
    {
        if(calosc[0]=='A'&&calosc[1]=='U'&&calosc[2]=='G')
        {
                          cout<<"Znaleziono sekwencje DNA. Zostanie ona odkodowana.\n";
                          cout<<endl<<calosc<<endl;
                          string dekod=calosc.substr(3,calosc.length()-6);
                          gotowiec=Odkoduj(dekod);
                          ZDNA=true;
        }
        else
        {
                            cout<<"Znaleziono tekst. Zostanie on zakodowany.\n";
                            cout<<calosc<<endl<<endl;
                            gotowiec=Zakoduj(calosc);
                            ZDNA=false;
        }
        Zapis(gotowiec,ZDNA);
        CzyCzyscWklej(&plikz);
    }
    else
    {
        cout<<"Plik wklej.txt pusty. Nie mozna wykonac operacji.\n";
        system("PAUSE");
    }
    return 0;
}
