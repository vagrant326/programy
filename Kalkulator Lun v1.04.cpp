#include <iostream> 
#include <stdlib.h> 
#include <math.h>
using namespace std;
float budynek (float cm,float ck,float cd,float potega,string nazwa,int kat)
{
      unsigned int p;
      float cL;
      string czasownik,wstep;
      if (kat==1)
      {
                 czasownik="wybudowac";
                 wstep="Ktory poziom";
      }
      else
      {
          if (kat==2)
          {
                     czasownik="zbadac";
                     wstep="Ktory poziom";
          }
          else
          if (kat==3 || kat==4)
          {
                     czasownik="zbudowac";
                     wstep="Ile";
          }
      }
      cout<<wstep<<" "<<nazwa<<" chcesz "<<czasownik<<"?"<<endl;
      cin>>p;
      cm=cm*(pow(potega,p));
      ck=ck*(pow(potega,p));
      cd=cd*(pow(potega,p));
      cL=(cm/4)+(ck/2)+cd;
      return cL;
}    
int main()
{
    float tm,th,td,mL,cL,bL,L,Lm,wm,wk,wd,cm,ck,cd,mm,md,mk,potega;
    int thv,tmv,tdv;
    unsigned int kat,rodzaj,p;
    // przed optymalizacja 670 linijek, po 356. odpadlo 47%;
    /* TODO (v326#5#): obliczanie ceny grawitonow line 178*/
    //m-mnoznik,W[]wydobycie,M[]magazyn,BLbrakujace,t czas, LM-luny na minute tv-zmienna tymczasowa czasu;
    cout<<"Witaj!\nKalkulator Lun v1.03.\nDzieki temu kalkulatorowi obliczysz kiedy bedzie Cie stac na dany obiekt,\nAby obliczyc ilosc wydobywanych Lun na godzine (L/h),\nw Menu wybierz Surowce i wpisz wartosci z wiersza ''Total''. Kropki pomin.\n\n"<<"Wpisz ilosc wydobywanego Metalu\n";
    cin>>wm;
    cout<<"\nWpisz ilosc wydobywanego Krysztalu\n";
    cin>>wk;
    cout<<"\nWpisz ilosc wydobywanego Deuteru\n";
    cin>>wd;
    L=(wm/4)+(wk/2)+wd;
    Lm=L/60;
    cout<<"\nWydobywasz "<<L<<" L/h\n\n"<<"czyli "<<Lm<<" L/min\n\n";
    cout<<"Aby obliczyc cene obiektu w Lunach, wpisz odpowiednie dane.\nWpisz rodzaj obiektu\n1=Budynek, 2=Technologia, 3=Flota, 4=Obrona, 5=Wlasny\n";
    cin>>kat;
    switch (kat)
    {
              case 1: {
                       cout<<"Wybierz rodzaj budynku\n1=Kopalnia metalu, 2=Kopalnia krysztalu, 3=Ekstraktor deuteru,"<<endl;
                       cout<<"4=Elektrownia sloneczna, 5=Elektrownia fuzyjna, 6=Fabryka robotow,"<<endl;
                       cout<<"7=Fabryka nanitow, 8=Stocznia, 9=Magazyn metalu, 10=Magazyn krysztalu,"<<endl;
                       cout<<"11=Zbiornik deuteru, 12=Laboratorium badawcze, 13=Terraformer,"<<endl;
                       cout<<"14=Depozyt sojuszniczy, 15=Stacja ksiezycowa, 16=Falanga czujnikow,"<<endl;
                       cout<<"17=Teleporter, 18=Silos rakietowy."<<endl;
                       cin>>rodzaj;
                       switch (rodzaj)
                       {
                              case 1: {
                                       //cena :1.5     
                                       cL=budynek(40,10,0,1.5,"Kopalni metalu",kat);
                                       break;}
                              case 2: {     
                                       cL=budynek(32,16,0,1.5,"Kopalni krysztalu",kat);
                                       break;}
                              case 3: {
                                       cL=budynek(150,50,0,1.5,"Ekstraktora deuteru",kat);
                                       break;}
                              case 4: {
                                       cL=budynek(50,20,0,1.5,"Elektrowni slonecznej",kat);
                                       break;}
                              case 5: {
                                       //cena :1.8
                                       cL=budynek(500,200,100,1.8,"Elektrowni fuzyjnej",kat);
                                       break;}
                              case 6: {
                                       cL=budynek(200,60,100,2,"Fabryki robotow",kat);
                                       break;}
                              case 7: {
                                       cL=budynek(500000,250000,50000,2,"Fabryki nanitow",kat);
                                       break;}             
                              case 8: {
                                       cL=budynek(200,100,50,2,"Stoczni",kat); 
                                       break;}
                              case 9: {
                                       cL=budynek(1000,0,0,2,"Magazynu metalu",kat);
                                       break;}
                              case 10: {
                                       cL=budynek(1000,500,0,2,"Magazynu krysztalu",kat);
                                       break;}
                              case 11: {
                                       cL=budynek(1000,1000,0,2,"Zbiornika deuteru",kat);
                                       break;}
                              case 12: {
                                       cL=budynek(100,200,100,2,"Laboratorium badawczego",kat);
                                       break;}
                              case 13: {
                                       cL=budynek(0,25000,50000,2,"Terraformera",kat);
                                       break;}
                              case 14: {
                                       cL=budynek(10000,20000,0,2,"Depozytu sojuszniczego",kat);
                                       float cm=10000,ck=20000,cd=0;
                                       break;}
                              case 15: {
                                       cL=budynek(10000,20000,10000,2,"Stacji ksiezycowej",kat);
                                       break;}
                              case 16: {
                                       cL=budynek(10000,20000,10000,2,"Falangi czujnikow",kat);
                                       break;}
                              case 17: {
                                       cL=budynek(1000000,2000000,1000000,2,"Teleportera",kat);
                                       break;}
                              case 18: {
                                       cL=budynek(10000,10000,500,2,"Silosu rakietowego",kat);
                                       break;}
                       }
              break;}
              case 2: {
                       cout<<"Wybierz rodzaj technologii\n1=Technologia szpiegowska, 2=T. komputerowa, 3=T. bojowa,"<<endl;
                       cout<<"4=T. ochronna, 5=Opancerzenie, 6=T. energetyczna, 7=T. nadprzestrzenna,"<<endl;
                       cout<<"8=Naped spalinowy, 9=N. impulsowy, 10=N. nadprzestrzenny,"<<endl;
                       cout<<"11=T. laserowa, 12=T. jonowa, 13=T. plazmowa,"<<endl;
                       cout<<"14=Miedzygalaktyczna Siec Badan Naukowych, 15=T. ekspedycji,\n16=Rozwoj grawitonow,"<<endl;
                       cin>>rodzaj;
                       switch (rodzaj)
                       {
                              case 1: {
                                       cL=budynek(100,500,100,2,"T. szpiegowskiej",kat);
                                       break;}
                              case 2: {
                                       cL=budynek(0,200,300,2,"T. komputerowej",kat);
                                       break;}
                              case 3: {
                                       cL=budynek(400,200,0,2,"T. bojowej",kat);
                                       break;}
                              case 4: {
                                       cL=budynek(100,300,0,2,"T. ochronnej",kat);
                                       break;}
                              case 5: {
                                       cL=budynek(500,0,0,2,"Opancerzenia",kat);
                                       break;}
                              case 6: {
                                       cL=budynek(0,400,200,2,"T. energetycznej",kat);
                                       break;}
                              case 7: {
                                       cL=budynek(0,2000,1000,2,"T. nadprzestrzennej",kat);
                                       break;}
                              case 8: {
                                       cL=budynek(300,0,300,2,"N. spalinowego",kat);
                                       break;}
                              case 9: {
                                       cL=budynek(1000,2000,3000,2,"N. impulsowego",kat);
                                       break;}
                              case 10: {
                                       cL=budynek(5000,10000,3000,2,"N. nadprzestrzennego",kat);
                                       break;}
                              case 11: {
                                       cL=budynek(100,50,0,2,"T. laserowej",kat);
                                       break;}
                              case 12: {
                                       cL=budynek(500,150,50,2,"T. jonowej",kat);
                                       break;}
                              case 13: {
                                       cL=budynek(1000,2000,5000,2,"T. plazmowej",kat);
                                       break;}
                              case 14: {
                                       cL=budynek(120000,200000,80000,2,"Miedzygalaktycznej Sieci Badan Naukowych",kat);
                                       break;}
                              case 15: {
                                       cL=budynek(100,500,100,2,"T. ekspedycji",kat);
                                       break;}
                              case 16: {
                                       float cm=0,ck=0,cd=0;
                                       cout<<"Ktory poziom Rozwoju grawitonow[x] chcesz zbadac?"<<endl;
                                       cin>>p;
                                       break;}
                       }
              break;}
              case 3: {
                       cout<<"Flota"<<endl;
                       cout<<"Wybierz rodzaj statku\n1=Maly transporter, 2=Duzy transporter, 3=Lekki mysliwiec,"<<endl;
                       cout<<"4=Ciezki mysliwiec, 5=Krazownik, 6=Okret wojenny,"<<endl;
                       cout<<"7=Statek kolonizacyjny, 8=Recykler, 9=Sonda szpiegowska,"<<endl;
                       cout<<"10=Bombowiec, 11=Satelita sloneczny, 12=Niszczyciel,"<<endl;
                       cout<<"13=Gwiazda Smierci, 14=Pancernik,"<<endl;
                       cin>>rodzaj;
                       switch (rodzaj)
                       {
                              case 1: {
                                       //ceny normalnie
                                       cL=budynek(2000,2000,0,1,"Malych transporterow",kat);
                                       break;}
                              case 2: {
                                       cL=budynek(6000,6000,0,1,"Duzych transporterow",kat);
                                       break;}
                              case 3: {
                                       cL=budynek(3000,1000,0,1,"Lekkich mysliwcow",kat);
                                       break;}
                              case 4: {
                                       cL=budynek(6000,4000,0,1,"Ciezkich mysliwcow",kat);
                                       break;}
                              case 5: {
                                       cL=budynek(20000,7000,2000,1,"Krazownikow",kat);
                                       break;}
                              case 6: {
                                       cL=budynek(45000,15000,0,1,"Okretow wojennych",kat);
                                       break;}
                              case 7: {
                                       cL=budynek(10000,20000,10000,1,"Statkow kolonizacyjnych",kat);
                                       break;}
                              case 8: {
                                       cL=budynek(10000,6000,2000,1,"Recyklerow",kat);
                                       break;}
                              case 9: {
                                       cL=budynek(0,1000,0,1,"Sond szpiegowskich",kat);
                                       break;}
                              case 10: {
                                       cL=budynek(50000,25000,15000,1,"Bombowcow",kat);
                                       break;}
                              case 11: {
                                       cL=budynek(0,2000,500,1,"Satelit slonczneych",kat);
                                       break;}
                              case 12: {
                                       cL=budynek(60000,50000,15000,1,"Niszczycieli",kat);
                                       break;}
                              case 13: {
                                       cL=budynek(5000000,4000000,1000000,1,"Gwiazd Smierci",kat);
                                       break;}
                              case 14: {
                                       cL=budynek(30000,40000,15000,1,"Pancernikow",kat);
                                       break;}
                       }
              break;}
              case 4: {
                       cout<<"Obrona"<<endl;
                       cout<<"Wybierz rodzaj obiektu\n1=Wyrzutnia rakiet, 2=Lekkie dzialo laserowe, 3=Ciezkie dzialo laserowe,"<<endl;
                       cout<<"4=Dzialo Gaussa, 5=Dzialo jonowe, 6=Wyrzutnia plazmy,"<<endl;
                       cout<<"7=Mala powloka ochronna, 8=Duza powloka ochronna, 9=Przeciwrakieta,"<<endl;
                       cout<<"10=Rakieta miedzyplanetarna,"<<endl;
                       cin>>rodzaj;
                       switch (rodzaj)
                       {
                              case 1: {
                                       cL=budynek(2000,0,0,1,"Wyrzutni rakiet",kat);
                                       break;}
                              case 2: {
                                       cL=budynek(1500,500,0,1,"Lekkich dzial laserowych",kat);
                                       break;}
                              case 3: {
                                       cL=budynek(6000,2000,0,1,"Ciezkich dzial laserowych",kat);
                                       break;}
                              case 4: {
                                       cL=budynek(20000,15000,2000,1,"Dzial Gaussa",kat);
                                       break;}
                              case 5: {
                                       cL=budynek(2000,6000,0,1,"Dzial jonowych",kat);
                                       break;}
                              case 6: {
                                       cL=budynek(50000,50000,30000,1,"Wyrzutni plazmy",kat);
                                       break;}
                              case 7: {
                                       cout<<"Mala powloka ochronna"<<endl;
                                       cL=7500;               
                                       break;}
                              case 8: {
                                       cout<<"Duza powloka ochronna"<<endl;
                                       cL=37500;              
                                       break;}
                              case 9: {
                                       cL=budynek(8000,0,2000,1,"Przeciwrakiet",kat);
                                       break;}
                              case 10: {
                                       cL=budynek(12500,2500,1000,1,"Rakiet miedzyplanetarnych",kat);
                                       break;}
              break;}
              break;}
              case 5: {
                       cout<<"Wlasny"<<endl;
                       cout<<"Wpisz potrzebna ilosc Metalu\n";
                       cin>>cm;
                       cout<<"\nWpisz potrzebna ilosc Krysztalu\n";
                       cin>>ck;
                       cout<<"\nWpisz potrzebna ilosc Deuteru\n";
                       cin>>cd;
                       cL=(cm/4)+(ck/2)+cd;
                       break;}
    }
                       cout<<"\nObiekt ten kosztuje "<<cL<<" Lun\n\n";
                       cout<<"Aby obliczyc ile czasu musisz jeszcze czekac,\nwpisz ilosc posiadanych surowcow.\nKropki pomin.\n\nWpisz ilosc posiadanego Metalu\n";
                       cin>>mm;
                       cout<<"\nWpisz ilosc posiadanego Krysztalu\n";
                       cin>>mk;
                       cout<<"\nWpisz ilosc posiadanego Deuteru\n";
                       cin>>md;
                       mL=(mm/4)+(mk/2)+md;
                       cout<<"\nPosiadasz "<<mL<<" Lun.\n";
                       bL=cL-mL;
                       cout<<"\nBrakuje ci "<<bL<<" Lun.\n\nPrzy wydobyciu o wielkosci "<<L<<" Lun na godzine\nna surowce bedziesz musial czekac jeszcze\n";
                       th=bL/L;
                       if (th>24)
                       {
                                td=(th/24);
                                tdv=td;
                                th=(th-(tdv*24));
                                thv=th;
                                if (tdv==1)
                                {
                                           cout<<tdv<<" dzien, "<<thv<<" godzin i ";
                                }
                                else
                                {
                                           cout<<tdv<<" dni, "<<thv<<" godzin i ";
                                }
                                thv=(thv*60);
                                th=(th*60);                                
                                tm=(th-thv);
                                tmv=tm;                                
                                cout<<tmv<<" minut.";
                       }
                       else
                       {
                           if (th<1)
                           {
                                    thv=th;
                                    thv=(thv*60);
                                    th=(th*60);                                
                                    tm=(th-thv);
                                    tmv=tm;
                                    cout<<tmv<<" minut.";
                           }
                           else
                           {
                                    thv=th;
                                    cout<<thv<<" godzin i ";
                                    thv=(thv*60);
                                    th=(th*60);                                
                                    tm=(th-thv);
                                    tmv=tm;
                                    cout<<tmv<<" minut.";
                           }
                       }
cout<<"\n\nKalkulator Lun by Vagrant326 (JiFProductions)\n\n";
system ("Pause"); 
return 0;
}
