#include"stdafx.h"
#include"time.h"
#include<iostream>
using namespace std;
unsigned int const n=1000000;
void bubble_sort(unsigned long int n, float *a)
{
	unsigned long int l,k;
	float p;
	l=n;
	do
	{
		k=0;
		l--;
		for(unsigned long int i=1;i<=l;i++)
		{
			if(a[i]>a[i+1])
			{
				p=a[i];
				a[i]=a[i+1];
				a[i+1]=p;
				k++;
			}
		}
	}
	while(k!=0);
}
int main()
{
	float *a=new float[n];
	int k;
	unsigned long int m;
	time_t t;
	clock_t tp,tk;
	double tc;
	srand((unsigned)time(&t));
	cout<<"Podaj ilosc wyrazow ciagu\n";
	cin>>m;
	do
	{
		cout<<"Podaj numer procedury:\n1 - Babelkowa\nPodaj k: ";
		cin>>k;
		if(k!=0)
		{
			cout<<"Ciag wylosowany, k="<<k<<endl;
			for(unsigned long int i=1;i<=m;i++)
			{
				a[i]=rand()%100000;
				cout<<a[i]<<'\t';
			}
		}
		cout<<endl;
		switch(k)
		{
		case 1:
			{
				cout<<"Babelkowa\n";
				tp=clock();
				bubble_sort(m,a);
				tk=clock();
				tc=(tk-tp)/double(CLOCKS_PER_SEC);
				cout<<"Czas obliczen= "<<tc<<endl;
				break;
			}
		}
		if(k!=0)
		{
			cout<<"Ciag podstawowy \n";
			for(unsigned long int i=1;i<=m;i++)
			{
				cout<<a[i]<<'\t';
			}
			cout<<"Czas obliczen: "<<tc<<endl;
		}
	}
	while(k!=0);
	return 0;
}
